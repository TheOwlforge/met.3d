# About
TODO: Summary of Thesis

# Met.3D
## 3D visualization of numerical ensemble weather prediction data

> Current, up-to-date information and documentation is available online at
> http://met3d.wavestoweather.de and http://met3d.readthedocs.org/
> Please refer to these websites for the latest documentation version.

Met.3D is an open-source visualization tool for interactive, three-dimensional
visualization of numerical ensemble weather predictions and similar numerical
atmospheric model datasets. The tool is implemented in C++ and OpenGL-4 and
runs on standard commodity hardware. It has originally been designed for
weather forecasting during atmospheric research field campaigns, however, is
not restricted to this application. Besides being used as a visualization tool,
Met.3D is intended to serve as a framework to implement and evaluate new 3D and
ensemble visualization techniques for the atmospheric sciences.

The Met.3D reference publication has been published in "Geoscientific
Model Development" and is available online:

   Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.:
   "Three-dimensional visualization of ensemble weather forecasts -- Part 1:
   The visualization tool Met.3D (version 1.0)", Geosci. Model Dev., 8,
   2329-2353, doi:10.5194/gmd-8-2329-2015, 2015.
   Available at: http://www.geosci-model-dev.net/8/2329/2015/gmd-8-2329-2015.html

Met.3D is developed at the Computer Graphics & Visualization Group, Technische
Universitaet Muenchen, Garching, Germany. We hope you find the tool useful for
your work, too. Please let us know about your experiences.


## Features:

Met.3D currently runs under Linux (we are working on a Windows version). Its
only "special" requirement is an OpenGL-4 capable graphics card. A standard
consumer (i.e. gaming) model works fine. Met.3D's current features include:

* Interactive 2D horizontal sections in a 3D context, including filled and line
  contours, pseudo-colour plots and wind barbs.
* Interactive 2D vertical sections in a 3D context along arbitrary waypoints,
  including filled and line contours.
* 3D isosurface volume renderer that supports multiple isosurfaces. Isosurfaces
  can be coloured according to an auxiliary variable.
* 3D direction volume rendering.
* Surface shadows and interactive vertical axes to improve spatial perception.
* Navigation for forecast initialisation and valid time and ensemble member.
* Interactive computation of ensemble statistical quantities (mean, standard
  deviation, probabilities, ...) of any ensemble data field.
* 3D trajectory rendering for trajectories computed by the LAGRANTO model
  (Sprenger and Wernli, Geosci. Model Dev., 2015).
* On-the-fly gridding of ensemble trajectories into 3D probability volumes.
* Support for 3D model data that is regular in the horizontal. In the vertical,
  both pressure levels and in particular sigma-hybrid-pressure-levels (ECMWF
  model) are natively supported.
* Data can be read from CF-compliant NetCDF files and from ECMWF GRIB files.
* Interactive visual analysis of probability volumes.
* Multithreaded data pipeline architecture.
* Modular architecture designed to allow the straightforward implementation
  of additional visualization/data processing/data analysis modules.


## Documentation:

User guide: An evolving user guide is available online at

                      http://met3d.readthedocs.org/


In addition to the reference publication, a second paper available online from
Geoscientific Model Development describes the specific application case of
forecasting warm conveyor belt situations with Met.3D:

   Rautenhaus, M., Grams, C. M., Schäfler, A., and Westermann, R.:
   "Three-dimensional visualization of ensemble weather forecasts --Part 2:
   Forecasting warm conveyor belt situations for aircraft-based field campaigns",
   Geosci. Model Dev., 8, 2355-2377, doi:10.5194/gmd-8-2355-2015, 2015.
   Available at: http://www.geosci-model-dev.net/8/2355/2015/gmd-8-2355-2015.html

A high-level description of the project can be found in the ECMWF Newsletter:

   Rautenhaus, M., Grams, C. M., Schäfler, A., and Westermann, R.:
   "GPU based interactive 3D visualization of ECMWF ensemble forecasts",
   ECMWF Newsletter, vol. 138 (Winter 2014), pp. 34-38, 2014.
   Available at: http://old.ecmwf.int/publications/newsletters/pdf/138.pdf


## Installation and configuration

Installation and configuration notes can be found online in the user
documentation at http://met3d.readthedocs.org/.
