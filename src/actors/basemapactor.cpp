/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2015      Michael Kern
**  Copyright 2017      Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "basemapactor.h"

// standard library imports
#include <iostream>
#include <cstdio>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <gdal_priv.h>
#include <QString>
#include <QFileDialog>

// local application imports
#include "util/mstopwatch.h"
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"


using namespace std;

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBaseMapActor::MBaseMapActor()
    : MRotatedGridSupportingActor(),
      MBoundingBoxInterface(this),
      texture(nullptr),
      textureUnit(-1),
      ssboBasemap(nullptr),
      ssboBasemapIndex(nullptr),
      numVerticesBasemap(0),
      colourSaturation(0.3),
      mapResolution(1)
{
    GDALAllRegister();

    bBoxConnection = new MBoundingBoxConnection(
        this, MBoundingBoxConnection::HORIZONTAL);


    // Create and initialise QtProperties for the GUI.
    // ===============================================
    beginInitialiseQtProperties();

    setActorType("Base map");
    setName(getActorType());

    loadMapProperty = addProperty(CLICK_PROPERTY, "load map",
                                  actorPropertiesSupGroup);

    filenameProperty = addProperty(STRING_PROPERTY, "map file",
                                   actorPropertiesSupGroup);
    properties->mString()->setValue(filenameProperty, "");
    filenameProperty->setEnabled(false);

    // Bounding box of the actor.
    actorPropertiesSupGroup->addSubProperty(bBoxConnection->getProperty());

    colourSaturationProperty = addProperty(DECORATEDDOUBLE_PROPERTY,
                                           "colour saturation",
                                           actorPropertiesSupGroup);
    properties->setDDouble(colourSaturationProperty, colourSaturation,
                           0., 1., 2., 0.1, " (0..1)");

    mapResolutionProperty = addProperty(DOUBLE_PROPERTY,
                                        "map resolution",
                                        actorPropertiesSupGroup);
    properties->setDouble(mapResolutionProperty, mapResolution, 0.01, 10, 2, 0.01);

    actorPropertiesSupGroup->addSubProperty(rotatedGridPropertiesSubGroup);

    endInitialiseQtProperties();
}

MBaseMapActor::~MBaseMapActor()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    if (ssboBasemap != nullptr) glRM->releaseGPUItem(ssboBasemap);
    if (ssboBasemapIndex != nullptr) glRM->releaseGPUItem(ssboBasemapIndex);
    if (texture) glRM->releaseGPUItem(texture);
    if (textureUnit >= 0) releaseTextureUnit(textureUnit);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

const uint8_t SHADER_VERTEX_ATTRIBUTE = 0;

const uint8_t SHADER_VALUE_ATTRIBUTE = 1;

void MBaseMapActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "loading shader programs\n" << flush);
    shaderProgram->compileFromFile_Met3DHome("src/glsl/basemap.fx.glsl");
    beginCompileShaders(1);
    compileShadersFromFileWithProgressDialog(
        computeMapProgram,
        "src/glsl/basemap_computemap.fx.glsl");
    endCompileShaders();
}

void MBaseMapActor::saveConfiguration(QSettings *settings)
{
    MRotatedGridSupportingActor::saveConfiguration(settings);

    settings->beginGroup(MBaseMapActor::getSettingsID());

    MBoundingBoxInterface::saveConfiguration(settings);
    settings->setValue("colourSaturation", float(colourSaturation));

    QString filename = properties->mString()->value(filenameProperty);
    settings->setValue("filename", filename);

    settings->endGroup();
}

void MBaseMapActor::loadConfiguration(QSettings *settings)
{
    MRotatedGridSupportingActor::loadConfiguration(settings);

    settings->beginGroup(MBaseMapActor::getSettingsID());

    MBoundingBoxInterface::loadConfiguration(settings);

    colourSaturation = settings->value("colourSaturation").toFloat();
    properties->mDDouble()->setValue(colourSaturationProperty, colourSaturation);

    const QString filename = settings->value("filename").toString();
    properties->mString()->setValue(filenameProperty, filename);

    settings->endGroup();

    if (isInitialized()) loadMap(filename.toStdString());
}

void MBaseMapActor::setFilename(QString filename)
{
    properties->mString()->setValue(filenameProperty, filename);
}

void MBaseMapActor::onBoundingBoxChanged()
{
    labels.clear();

    if (suppressActorUpdates())
    {
        return;
    }

    // Only adapt bounding box for rotated grids if base map is connected to
    // a bounding box.
    if (bBoxConnection->getBoundingBox() != nullptr)
    {
        adaptBBoxForRotatedGrids();
        generateBasemapGeometry();
    }

    emitActorChangedSignal();
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MBaseMapActor::initializeActorResources()
{
    // Bind the texture object to this unit.
    textureUnit = assignTextureUnit();

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    QString filename = properties->mString()->value(filenameProperty);
    loadMap(filename.toStdString());

    // Load shader program if the returned program is new.
    bool loadShaders = false;

    loadShaders |= glRM->generateEffectProgram("mapactor_shader", shaderProgram);
    loadShaders |= glRM->generateEffectProgram("mapactor_computemap_shader", computeMapProgram);

    if (loadShaders) reloadShaderEffects();

    adaptBBoxForRotatedGrids();
    generateBasemapGeometry();
}

void MBaseMapActor::onQtPropertyChanged(QtProperty *property)
{
    if (property == loadMapProperty)
    {
        // open file dialog to select geotiff file
        QString filename = QFileDialog::getOpenFileName(
            nullptr, tr("Open GeoTiff Map"),
            "/home/",
            tr("Tiff Image Files (*.tif *.geotiff)"));

        properties->mString()->setValue(filenameProperty, filename);
    }

    else if (property == filenameProperty)
    {
        QString filename = properties->mString()->value(filenameProperty);
        if (suppressActorUpdates()) return;
        if (filename.isEmpty()) return;

        loadMap(filename.toStdString());
        emitActorChangedSignal();
    }

    else if (property == colourSaturationProperty)
    {
        colourSaturation =
            properties->mDDouble()->value(colourSaturationProperty);
        emitActorChangedSignal();
    }

    else if (property == mapResolutionProperty)
    {
        mapResolution = properties->mDouble()->value(mapResolutionProperty);
        if (suppressActorUpdates()) return;
        generateBasemapGeometry();
        emitActorChangedSignal();
    }

    if (property == enableGridRotationProperty)
    {
        enableGridRotation =
            properties->mBool()->value(enableGridRotationProperty);
        if (suppressActorUpdates()) return;
        generateBasemapGeometry();
        emitActorChangedSignal();
    }

    else if (property == rotatedNorthPoleProperty)
    {
        rotatedNorthPole =
            properties->mPointF()->value(rotatedNorthPoleProperty);
        if (suppressActorUpdates()) return;
        if (enableGridRotation)
        {
            generateBasemapGeometry();
            emitActorChangedSignal();
        }
    }

    else if (property == projectionProperty)
    {
        if (suppressActorUpdates()) return;
        generateBasemapGeometry();
        emitActorChangedSignal();
    }
}

void MBaseMapActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (texture != nullptr && bBoxConnection->getBoundingBox() != nullptr)
    {
        // Bind shader program.
        shaderProgram->bindProgram("Basemap");

        QVector4D dataVec4(llcrnrlon, llcrnrlat, urcrnrlon, urcrnrlat);
        shaderProgram->setUniformValue("cornersData", dataVec4);
        shaderProgram->setUniformValue("mvpMatrix",
                                       *(sceneView->getModelViewProjectionMatrix()));

        // Bind texture and use correct texture unit in shader.
        texture->bindToTextureUnit(textureUnit);
        shaderProgram->setUniformValue("mapTexture", textureUnit);
        shaderProgram->setUniformValue("colourIntensity", colourSaturation);

        if (projectionMethod == EQUIRECTANGULAR)
        {
            shaderProgram->setUniformValue("inverse", true);
        }
        else if (projectionMethod == STEREOGRAPHIC)
        {
            shaderProgram->setUniformValue("inverse", false);
        }

        //bind index and shader storage buffer.
        glBindBuffer(GL_ARRAY_BUFFER, ssboBasemap->getBufferObject());
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ssboBasemapIndex->getBufferObject());
        CHECK_GL_ERROR;

        glVertexAttribPointer(
            SHADER_VERTEX_ATTRIBUTE,
            2, GL_FLOAT,
            GL_FALSE,
            4 * sizeof(float),
            (const GLvoid *) 0);

        glVertexAttribPointer(
            SHADER_VALUE_ATTRIBUTE,
            2, GL_FLOAT,
            GL_FALSE,
            4 * sizeof(float),
            (const GLvoid *) (2 * sizeof(float)));

        glEnableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE);
        glEnableVertexAttribArray(SHADER_VALUE_ATTRIBUTE);

        CHECK_GL_ERROR;

        // Draw map.
        glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrame ? GL_LINE : GL_FILL);
        //glDrawArrays(GL_TRIANGLES, 0, numVerticesBasemap); CHECK_GL_ERROR;
        glDrawElements(GL_TRIANGLES, numVerticesBasemap, GL_UNSIGNED_INT, (GLvoid *) 0);
        CHECK_GL_ERROR;

        // Unbind VBO.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        // Unbind IBO.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
    }

}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MBaseMapActor::loadMap(std::string filename)
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    if (filename.empty())
    {
        LOG4CPLUS_ERROR(mlog, "Given GeoTiff filename is empty. "
                              "Cannot read file.");
        return;
    }

    LOG4CPLUS_DEBUG(mlog, "Reading world map image from GeoTiff file <" <<
                                                                        filename << ">..." << flush);

    // open the raster dataset and store it in a GDALDataset object
    GDALDataset *tiffData = static_cast<GDALDataset *>(
        GDALOpen(filename.c_str(), GA_ReadOnly));

    // GetDescription() <- filename
    // GetRaster Size() <- dimensions lot/lan
    // GetRasterCount() <- number of raster bands (3 for rgb)
    // GetDriver()->GetDescription() <- data type

    if (tiffData == nullptr)
    {
        LOG4CPLUS_ERROR(mlog, "Cannot open GeoTIFF file <"
            << filename << ">.");
        return;
    }

    std::string datasetType = tiffData->GetDriver()->GetDescription();

    if (datasetType != "GTiff")
    {
        LOG4CPLUS_ERROR(mlog, "Raster dataset <"
            << filename << "> is not of type GeoTiff.");
        return;
    }

    // get the geo-spatial translation of the current raster dataset
    QVector<double> geoData(6);
    // 0 - top left x
    // 1 - w-e pixel resolution
    // 2 - 0
    // 3 - top left y
    // 4 - 0
    // 5 - n-s pixel resolution (negative)

    tiffData->GetGeoTransform(&geoData[0]);

    // calculate lat/lon corners of loaded map and store them in class
    const int32_t longitudeDim = tiffData->GetRasterXSize();
    const int32_t latitudeDim = tiffData->GetRasterYSize();
    const int32_t colorDim = tiffData->GetRasterCount();

    llcrnrlat = geoData[3] + geoData[5] * latitudeDim;
    llcrnrlon = geoData[0];
    urcrnrlat = geoData[3];
    urcrnrlon = geoData[0] + geoData[1] * longitudeDim;

    LOG4CPLUS_DEBUG(mlog, "\tLongitude range: " << llcrnrlon << " - "
                                                << urcrnrlon << flush);
    LOG4CPLUS_DEBUG(mlog, "\tLatitude range: " << llcrnrlat << " - "
                                               << urcrnrlat << flush);

    // read the color data by using bands
    // we need three bands for every color channel
    GDALRasterBand *bandRed;
    GDALRasterBand *bandGreen;
    GDALRasterBand *bandBlue;

    bandRed = tiffData->GetRasterBand(1);
    bandGreen = tiffData->GetRasterBand(2);
    bandBlue = tiffData->GetRasterBand(3);

    // GDALGetDataTypeName(band->GetRasterDataType()) <- datatype
    // DALGetColorInterpretationName(band->GetColorInterpretation()) <- color channel

    std::string dataType = GDALGetDataTypeName(bandRed->GetRasterDataType());

    if (dataType != "Byte")
    {
        LOG4CPLUS_ERROR(mlog, "Raster dataset has no data of type Byte.");
        return;
    }

    // image data
    const int32_t imgSizeX = longitudeDim * colorDim;
    const int32_t imgSize = imgSizeX * latitudeDim;

    std::vector<GLbyte> tiffImg;
    // changed reserve to resize because of "vector subscript out of range"
    // error on windows
    tiffImg.resize(imgSize * sizeof(GLbyte));
    //GLbyte* tiffImg = new GLbyte[imgSize * sizeof(GLbyte)];

    std::string file = filename.substr(0, filename.find_last_of("."));
    std::string cacheFilename = file + ".ctif";

    // print out some information
    LOG4CPLUS_DEBUG(mlog, "\tMap texture size (lon/lat/col): " << longitudeDim
                                                               << "x" << latitudeDim << "x" << colorDim);

    // print out some information
    LOG4CPLUS_DEBUG(mlog, "\tParsing color data..." << flush);


    // if we can found a cache file
    FILE *cacheFile = fopen(cacheFilename.c_str(), "rb");
    if (cacheFile != nullptr)
    {
        LOG4CPLUS_DEBUG(mlog, "\tFound and using cache color data <"
            << cacheFilename << ">...");

        // obtain binary data from the exisiting file
        fread(&tiffImg[0], sizeof(GLbyte), imgSize, cacheFile);
        fclose(cacheFile);
    }
    else
    {
        // fetch the raster data of all color components
        std::vector<GLbyte> rasterRed(longitudeDim * latitudeDim);
        std::vector<GLbyte> rasterGreen(longitudeDim * latitudeDim);
        std::vector<GLbyte> rasterBlue(longitudeDim * latitudeDim);
        // load whole raster data into buffers
        bandRed->RasterIO(GF_Read,
                          0,
                          0,
                          longitudeDim,
                          latitudeDim,
                          &rasterRed[0],
                          longitudeDim,
                          latitudeDim,
                          GDT_Byte,
                          0,
                          0);
        bandGreen->RasterIO(GF_Read,
                            0,
                            0,
                            longitudeDim,
                            latitudeDim,
                            &rasterGreen[0],
                            longitudeDim,
                            latitudeDim,
                            GDT_Byte,
                            0,
                            0);
        bandBlue->RasterIO(GF_Read,
                           0,
                           0,
                           longitudeDim,
                           latitudeDim,
                           &rasterBlue[0],
                           longitudeDim,
                           latitudeDim,
                           GDT_Byte,
                           0,
                           0);

        // copy the color components into the image buffer
        for (int i = 0; i < longitudeDim * latitudeDim; ++i)
        {
            const uint32_t tiffID = i * colorDim;

            tiffImg[tiffID + 0] = rasterRed[i];
            tiffImg[tiffID + 1] = rasterGreen[i];
            tiffImg[tiffID + 2] = rasterBlue[i];
        }

        LOG4CPLUS_DEBUG(mlog, "\tCaching color data into file <"
            << cacheFilename << ">...");

        // write out a cache file to enhance the loading process
        cacheFile = fopen(cacheFilename.c_str(), "wb");
        fwrite(&tiffImg[0], sizeof(GLbyte), imgSize, cacheFile);
        fclose(cacheFile);
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "GeoTIFF read in "
        << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
        << " seconds.\n" << flush);
#endif

    // at the end close the tiff file
    GDALClose(tiffData);

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (texture == nullptr)
    {
        QString textureID = QString("baseMap_#%1").arg(getID());
        texture = new GL::MTexture(textureID, GL_TEXTURE_2D, GL_RGB8,
                                   longitudeDim, latitudeDim);

        if (!glRM->tryStoreGPUItem(texture))
        {
            delete texture;
            texture = nullptr;
        }
    }
    if (texture)
    {
        texture->updateSize(longitudeDim, latitudeDim);

        glRM->makeCurrent();
        texture->bindToLastTextureUnit();

        // Set texture parameters: wrap mode and filtering (RTVG p. 64).
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        // DEBUG
        //std::cout << longitudeDim * latitudeDim * colorDim << std::endl;
        //GLint size;
        //glGetIntegerv(GL_MAX_TEXTURE_SIZE,&size);
        //std::cout << size << std::endl << flush;

        // Upload data array to GPU.
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                     longitudeDim, latitudeDim,
                     0, GL_RGB, GL_UNSIGNED_BYTE, &tiffImg[0]);
        CHECK_GL_ERROR;
    }
}

void MBaseMapActor::adaptBBoxForRotatedGrids()
{
    double right, left, lower, upper;
    // Bounding box covers the full "east wast extend" of the sphere.
    if (bBoxConnection->eastWestExtent() >= 360.)
    {
        left = -180.;
        right = 180.;
    }
    else
    {
        // Map values greater than 360 and smaller than -360 to one of their
        // representative in the interval [-360, 360]

        left = fmod(bBoxConnection->westLon(), 360.);
        right = fmod(bBoxConnection->eastLon(), 360.);

        // Map left and right to the interval [-180, 180]

        if (left > 180.)
        {
            left -= 360.;
        }
        else if (left < -180.)
        {
            left += 360.;
        }
        if (right > 180.)
        {
            right -= 360.;
        }
        else if (right < -180.)
        {
            right += 360.;
        }

    }
    // Bounding box covers the full "north south extend" of the sphere.
    if (bBoxConnection->northSouthExtent() >= 360.)
    {
        lower = -90.;
        upper = 90.;
    }
    else
    {
        // Map values greater than 360 and smaller than -360 to one of their
        // representative in the interval [-360, 360]

        lower = fmod(bBoxConnection->southLat(), 360.);
        upper = fmod(bBoxConnection->northLat(), 360.);

        // Map lower and upper to the interval [-90, 90] by mapping the values
        // at the "backside" of the sphere to -90 and 90 respectively.

        if (lower > 180.)
        {
            lower -= 360.;
        }
        else if (lower < -180.)
        {
            lower += 360.;
        }
        if (upper > 180.)
        {
            upper -= 360.;
        }
        else if (upper < -180.)
        {
            upper += 360.;
        }
        if (lower > 90.)
        {
            lower = 90.;
        }
        else if (lower < -90.)
        {
            lower = -90.;
        }
        if (upper > 90.)
        {
            upper = 90.;
        }
        else if (upper < -90.)
        {
            upper = -90.;
        }
    }
    bboxForRotatedGrids = QVector4D(left, lower, right, upper);
}

void MBaseMapActor::generateBasemapGeometry()
{
    // Generate nothing if no bounding box is available.
    if (bBoxConnection->getBoundingBox() == nullptr)
    {
        return;
    }

#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    // Make sure that "glResourcesManager" is the currently active context,
    // otherwise glDrawArrays on the VBO generated here will fail in any other
    // context than the currently active. The "glResourcesManager" context is
    // shared with all visible contexts, hence modifying the VBO there works
    // fine.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    glRM->makeCurrent();

    LOG4CPLUS_DEBUG(mlog, "generating basemap geometry\n" << flush);

    int numLon;
    int numLat;
    float mapResolutionX;
    float mapResolutionY;
    float centralLon = enableGridRotation ? rotatedNorthPole.x() : 0.f;
    float centralLat = enableGridRotation ? rotatedNorthPole.y() : 0.f;
    if (projectionMethod == MActor::EQUIRECTANGULAR && !enableGridRotation)
    {
        numLon = 2;
        numLat = 2;
        mapResolutionX = abs(bboxForRotatedGrids.z() - bboxForRotatedGrids.x());
        mapResolutionY = abs(bboxForRotatedGrids.w() - bboxForRotatedGrids.y());
    }
    else
    {
        numLon = floor(abs(bboxForRotatedGrids.z() - bboxForRotatedGrids.x()) / mapResolution) + 1;
        numLat = floor(abs(bboxForRotatedGrids.w() - bboxForRotatedGrids.y()) / mapResolution) + 1;
        mapResolutionX = mapResolution;
        mapResolutionY = mapResolution;
    }

    // Create a shader storage buffer
    const int MAX_VERTICES = numLon * numLat;

    if (ssboBasemap == nullptr)
    {
        const QString ssboBasemapID =
            QString("basemap_ssbo_vertices_#%1").arg(myID);
        ssboBasemap = new GL::MShaderStorageBufferObject(
            ssboBasemapID, sizeof(QVector4D), MAX_VERTICES);

        if (glRM->tryStoreGPUItem(ssboBasemap))
        {
            // Obtain reference to SSBO item.
            ssboBasemap = static_cast<GL::MShaderStorageBufferObject *>(
                glRM->getGPUItem(ssboBasemapID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "WARNING: cannot store buffer for basemap");
            delete ssboBasemap;
            ssboBasemap = nullptr;
            return;
        }
    }
    else
    {
        ssboBasemap->updateSize(MAX_VERTICES);
    }

    QVector<QVector4D> initData(MAX_VERTICES, QVector4D(-1, -1, -1, -1));
    ssboBasemap->upload(initData.data(), GL_DYNAMIC_COPY);

    computeMapProgram->bindProgram("ComputeBasemap");
    // set projection variables.
    computeMapProgram->setUniformValue("centralLon", centralLon);
    computeMapProgram->setUniformValue("centralLat", centralLat);
    computeMapProgram->setUniformValue("projectionMethod", GLuint(projectionMethod));
    if (projectionMethod == EQUIRECTANGULAR)
    {
        computeMapProgram->setUniformValue("projectionDirection", 1);
    }
    else if (projectionMethod == STEREOGRAPHIC)
    {
        computeMapProgram->setUniformValue("projectionDirection", 1);
    }
    // set bounding box.
    computeMapProgram->setUniformValue("cornersBox", bboxForRotatedGrids);
    // set grid resolution.
    computeMapProgram->setUniformValue("mapResolutionX", mapResolutionX);
    computeMapProgram->setUniformValue("mapResolutionY", mapResolutionY);
    computeMapProgram->setUniformValue("numLon", numLon);
    computeMapProgram->setUniformValue("numLat", numLat);

    // Bind the SSBO to the binding index 0.
    ssboBasemap->bindToIndex(0);

    const int CS_GROUP_SIZE = 32;
    int dispatchLon = numLon / CS_GROUP_SIZE + 1;
    int dispatchLat = numLat / CS_GROUP_SIZE + 1;
    // two dimensions for grid.
    glDispatchCompute(dispatchLon, dispatchLat, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    // construct the index buffer.
    const int MAX_VERTICES_INDEX = (numLon - 1) * (numLat - 1) * 6;

    if (ssboBasemapIndex == nullptr)
    {
        const QString ssboBasemapIndexID =
            QString("basemap_ssbo_indices_#%1").arg(myID);
        ssboBasemapIndex = new GL::MShaderStorageBufferObject(
            ssboBasemapIndexID, sizeof(GLuint), MAX_VERTICES_INDEX);

        if (glRM->tryStoreGPUItem(ssboBasemapIndex))
        {
            // Obtain reference to SSBO item.
            ssboBasemapIndex = static_cast<GL::MShaderStorageBufferObject *>(
                glRM->getGPUItem(ssboBasemapIndexID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "WARNING: cannot store index buffer for basemap");
            delete ssboBasemapIndex;
            ssboBasemapIndex = nullptr;
            return;
        }
    }
    else
    {
        ssboBasemapIndex->updateSize(MAX_VERTICES_INDEX);
    }

    dispatchLon = (numLon - 1) / CS_GROUP_SIZE + 1;
    dispatchLat = (numLat - 1) / CS_GROUP_SIZE + 1;

    QVector<GLuint> indexData(MAX_VERTICES_INDEX, 1);
    ssboBasemapIndex->upload(indexData.data(), GL_DYNAMIC_COPY);

    computeMapProgram->bindProgram("ComputeBasemapIndex");
    // set projection variables.
    computeMapProgram->setUniformValue("centralLon", centralLon);
    computeMapProgram->setUniformValue("centralLat", centralLat);
    computeMapProgram->setUniformValue("projectionMethod", GLuint(projectionMethod));
    computeMapProgram->setUniformValue("rotated", enableGridRotation);
    if (projectionMethod == EQUIRECTANGULAR)
    {
        computeMapProgram->setUniformValue("checkConnection", false);
    }
    else if (projectionMethod == STEREOGRAPHIC)
    {
        computeMapProgram->setUniformValue("checkConnection", true);
    }
    // set grid resolution.
    computeMapProgram->setUniformValue("numLon", numLon);
    computeMapProgram->setUniformValue("numLat", numLat);

    // Bind the map to the binding index 0 and the indices to index 1.
    ssboBasemap->bindToIndex(0);
    ssboBasemapIndex->bindToIndex(1);

    // two dimensions for grid.
    glDispatchCompute(dispatchLon, dispatchLat, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    numVerticesBasemap = indexData.size();

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "Basemap computed in "
        << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
        << " seconds.\n" << flush);
#endif
}

} // namespace Met3D
