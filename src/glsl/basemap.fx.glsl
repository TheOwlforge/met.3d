/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2015-2017 Michael Kern
**  Copyright 2015-2017 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4 mvpMatrix;

// bounding box corners
uniform vec4 cornersData; //(llcrnrlon, llcrnrlat, urcrnrlon, urcrnrlat)

uniform sampler2D mapTexture;
uniform float colourIntensity; // 0..1 with 1 = rgb texture used, 0 = grey scales

uniform bool inverse;


/*****************************************************************************
 ***                               UTIL
 *****************************************************************************/

// Changes the intensity of the colour value given in rgbaColour according to
// the scale given in colourIntensity [0 -> gray, 1 -> unchanged saturation].
vec4 adaptColourIntensity(vec4 rgbaColour)
{
    // If colourIntensity < 1 turn RGB texture into grey scales. See
    // http://stackoverflow.com/questions/687261/converting-rgb-to-grayscale-intensity
    vec3 rgbColour = rgbaColour.rgb;
    vec3 grey = vec3(0.2989, 0.5870, 0.1140);
    vec3 greyColour = vec3(dot(rgbColour, grey));
    return vec4(mix(greyColour, rgbColour, colourIntensity), rgbaColour.a);
}

//computes texture coordinates from geographic coordinates.
//the longitude must be between -180 and 180.
//the latitude must be between -90 and 90.
vec2 mapToTexCoords(in vec2 position)
{
    vec2 texCoord;
    texCoord.x = (position.x - cornersData.x) / (cornersData.z - cornersData.x);
    texCoord.y = (cornersData.w - position.y) / (cornersData.w - cornersData.y);
    return texCoord;
}

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSmain(in vec2 pos : 0, in vec2 tex : 1, out vec2 texCoord)
{
    if (inverse)
    {
        gl_Position = mvpMatrix * vec4(tex, 0, 1.);
        texCoord = mapToTexCoords(pos);
    }
    else
    {
        gl_Position = mvpMatrix * vec4(pos, 0, 1.);
        texCoord = mapToTexCoords(tex);
    }
}

/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSmain(in vec2 texCoord, out vec4 fragColour)
{
    vec4 rgbaColour = vec4(texture(mapTexture, texCoord).rgb, 1);

    fragColour = adaptColourIntensity(rgbaColour);
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Basemap
{
    vs(330)=VSmain();
    fs(330)=FSmain();
};
