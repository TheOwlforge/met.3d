/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/
// include functions for projections
#include "projections.glsl"


/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

const float MISSING_VALUE = -999.E9;


/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/

interface VStoFS
{
    smooth float  flag;   // if this flag is set to < 0, the fragment
                          // shader should discard the fragment (for
                          // invalid vertices beneath the surface)
    smooth vec2   geoPosition; // holds the lat/lon coordinates of the fragment in
                               // world space coordinates
    smooth float  scalar; // the scalar data that this vertex holds,
                          // the value shall be perspectively correct
                          // interpolated to the fragment shader
    //smooth vec3   trianglePosition;
    //flat   vec3   normalCorners[3];
    //flat   vec3   sphericalPosCorners[3];
    //flat   vec2   geoPosCorners[3];
    //flat   vec3   scalarCorners;
};


/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

uniform sampler1D latLonAxesData;    // 1D texture that holds both lon and lat
uniform int       latOffset;         // index at which lat axis data starts
uniform float     worldZ;            // world z coordinate of this section
uniform mat4      mvpMatrix;         // model-view-projection

layout(r32f)
uniform image2D   crossSectionGrid;  // 2D data grid
uniform int       iOffset;           // grid index offsets if only a subregion
uniform int       jOffset;           //   of the grid shall be rendered
uniform vec2      bboxLons;          // western and eastern lon of the bbox

uniform float     shiftForWesternLon; // shift in multiples of 360 to get the
                                      // correct starting position (depends on
                                      // the distance between the left border of
                                      // the grid and the left border of the bbox)
//projection Variables
uniform int   projectionMethod;
uniform float centralLon;
uniform float centralLat;
uniform int   pos;

shader VSmain(out VStoFS Output)
{
    // Compute grid indices (i, j) of the this vertex from vertex and instance
    // ID (see notes 09Feb2012).
    int i = int(gl_VertexID / 2) + pos;
    int j = bool(gl_VertexID & 1) ? (gl_InstanceID + 1) : gl_InstanceID;

    int numLons = latOffset;

    // In case of repeated grid region parts, this shift places the vertex to
    // the correct global position. This is necessary since the modulo operation
    // maps repeated parts to the same position. It contains the factor 360.
    // needs to be multiplied with to place the vertex correctly.
    float numGlobalLonShifts = floor((i + iOffset) / latOffset);

    i = (i + iOffset) % numLons;
    j = j + jOffset;

    // Fetch lat/lon values from texture to obtain the position of this vertex
    // in world space.
    float lon = texelFetch(latLonAxesData, i, 0).a;
    float lat = texelFetch(latLonAxesData, j + latOffset, 0).a;

    lon += (numGlobalLonShifts * 360.) + shiftForWesternLon;

//    vec2 geoPosition = vec2(lon, lat);
//    vec2 mapProjection = projectGeoToMap(geoPosition, vec2(centralLon, centralLat), projectionMethod);
//    vec3 vertexPosition = vec3(mapProjection, worldZ);

    // Convert the position from world to clip space.
//    gl_Position = mvpMatrix * vec4(vertexPosition, 1);

    // NOTE: imageLoad requires "targetGrid" to be defined with the
    // layout qualifier, otherwise an error "..no overload function can
    // be found: imageLoad(struct image2D, ivec2).." is raised.
    vec4 data = imageLoad(crossSectionGrid, ivec2(i, j));
    Output.scalar = data.r;
    Output.geoPosition = vec2(lon, lat);

    if (Output.scalar != MISSING_VALUE) Output.flag = 0.; else Output.flag = -100.;
}

shader VSproj(out VStoFS Output)
{
    // Compute grid indices (i, j) of the this vertex from vertex and instance
    // ID (see notes 09Feb2012).
    int i = int(gl_VertexID / 2);
    int j = bool(gl_VertexID & 1) ? (gl_InstanceID + 1) : gl_InstanceID;

    int numLons = latOffset;

    // In case of repeated grid region parts, this shift places the vertex to
    // the correct global position. This is necessary since the modulo operation
    // maps repeated parts to the same position. It contains the factor 360.
    // needs to be multiplied with to place the vertex correctly.
    float numGlobalLonShifts = floor((i + iOffset) / latOffset);

    i = (i + iOffset) % numLons;
    j = j + jOffset;

    // Fetch lat/lon values from texture to obtain the position of this vertex
    // in world space.
    float lon = texelFetch(latLonAxesData, i, 0).a;
    float lat = texelFetch(latLonAxesData, j + latOffset, 0).a;

    lon += (numGlobalLonShifts * 360.) + shiftForWesternLon;

    vec2 geoPosition = vec2(lon, lat);
    vec2 mapProjection = projectGeoToMap(geoPosition, vec2(centralLon, centralLat), projectionMethod);
    vec3 vertexPosition = vec3(mapProjection, worldZ);

    // Convert the position from world to clip space.
    gl_Position = mvpMatrix * vec4(vertexPosition, 1);

    // NOTE: imageLoad requires "targetGrid" to be defined with the
    // layout qualifier, otherwise an error "..no overload function can
    // be found: imageLoad(struct image2D, ivec2).." is raised.
    vec4 data = imageLoad(crossSectionGrid, ivec2(i, j));
    Output.scalar = data.r;
    Output.geoPosition = vec2(lon, lat);

    if (Output.scalar != MISSING_VALUE) Output.flag = 0.; else Output.flag = -100.;
}


/*****************************************************************************
 ***                          GEOMETRY SHADER
 *****************************************************************************/

shader TCmain (in VStoFS Input[], out VStoFS Output[])
{
    Output[gl_InvocationID].geoPosition = Input[gl_InvocationID].geoPosition;
    Output[gl_InvocationID].scalar = Input[gl_InvocationID].scalar;
    Output[gl_InvocationID].flag = Input[gl_InvocationID].flag;
    if (gl_InvocationID == 0) {
        gl_TessLevelOuter[0] = 10;
        gl_TessLevelOuter[1] = 10;
        gl_TessLevelOuter[2] = 10;
        gl_TessLevelOuter[3] = 10;
        gl_TessLevelInner[0] = 10;
        gl_TessLevelInner[1] = 10;
    }
}

shader TEmain (in VStoFS Input[], out VStoFS Output)
{
    if (Input[0].flag == -100 || Input[0].flag == -100 || Input[0].flag == -100 || Input[0].flag == -100)
    {
        Output.flag = -100.;
    }
    else
    {
        Output.flag = 0.;
    }

    vec2 top = mix(Input[0].geoPosition, Input[1].geoPosition, gl_TessCoord.x);
    vec2 bottom = mix(Input[2].geoPosition, Input[3].geoPosition, gl_TessCoord.x);
    vec2 position = mix(top, bottom, gl_TessCoord.y);
    Output.geoPosition = position;

    if(interpolationMethod == LINEAR)
    {
        float topS = mix(Input[0].scalar, Input[1].scalar, gl_TessCoord.x);
        float bottomS = mix(Input[2].scalar, Input[3].scalar, gl_TessCoord.x);
        float scalar = mix(topS, bottomS, gl_TessCoord.y);
        Output.scalar = scalar;
    }
    else if (interpolationMethod == SPHERICAL)
    {
        float topS = slerp(Input[0].scalar, Input[1].scalar, gl_TessCoord.x);
        float bottomS = slerp(Input[2].scalar, Input[3].scalar, gl_TessCoord.x);
        float scalar = slerp(topS, bottomS, gl_TessCoord.y);
        Output.scalar = scalar;
    }
    else if (interpolationMethod == DISTANCE)
    {
        float powerParameter = -2;

        float h1 = distanceUnitSphere(Input[0].geoPosition, position);
        float h2 = distanceUnitSphere(Input[1].geoPosition, position);
        float h3 = distanceUnitSphere(Input[2].geoPosition, position);
        float h4 = distanceUnitSphere(Input[3].geoPosition, position);
        if (abs(h1) < 9.e-14)
        {
            Output.scalar = Input[0].scalar;
        }
        else if (abs(h2) < 9.e-14)
        {
            Output.scalar = Input[1].scalar;
        }
        else if (abs(h3) < 9.e-14)
        {
            Output.scalar = Input[2].scalar;
        }
        else if (abs(h4) < 9.e-14)
        {
            Output.scalar = Input[3].scalar;
        }
        else
        {
            float p1 = pow(h1, powerParameter);
            float p2 = pow(h2, powerParameter);
            float p3 = pow(h3, powerParameter);
            float p4 = pow(h4, powerParameter);

            float sum = p1 + p2 + p3 + p4;

            float w1 = p1 / sum;
            float w2 = p2 / sum;
            float w3 = p3 / sum;
            float w4 = p4 / sum;

            Output.scalar = w1 * Input[0].scalar
                            + w2 * Input[1].scalar
                            + w3 * Input[2].scalar
                            + w4 * Input[3].scalar;
        }
    }

    vec2 projection = projectGeoToMap(position,
                                      vec2(centralLon, centralLat),
                                      projectionMethod);
    gl_Position = mvpMatrix * vec4(projection, worldZ, 1.);
}

///*****************************************************************************
// ***                          GEOMETRY SHADER
// *****************************************************************************/
//
//shader GSmain(in VStoFS Input[], out VStoFS Output)
//{
//    //convert corner geoPos to spherical coordinates
//    vec3 corner1 = toSpherical(Input[0].geoPosition);
//    vec3 corner2 = toSpherical(Input[1].geoPosition);
//    vec3 corner3 = toSpherical(Input[2].geoPosition);
//    //compute normals
//    vec3 n1 = normalize(cross(corner2, corner3));
//    vec3 n2 = normalize(cross(corner3, corner1));
//    vec3 n3 = normalize(cross(corner1, corner2));
//    mat3 corners = mat3(corner1, corner2, corner3);
//    float detCorners = determinant(corners);
//    mat3 m1 = mat3(n1, corner2, corner3);
//    mat3 m2 = mat3(corner1, n2, corner3);
//    mat3 m3 = mat3(corner1, corner2, n3);
//    if(sign(detCorners) != sign(determinant(m1)))
//    {
//        n1 = -1 * n1;
//    }
//    if(sign(detCorners) != sign(determinant(m2)))
//    {
//        n2 = -1 * n2;
//    }
//    if(sign(detCorners) != sign(determinant(m3)))
//    {
//        n3 = -1 * n3;
//    }
//
//    gl_Position = gl_in[0].gl_Position;
//    Output.flag = Input[0].flag;
//    Output.geoPosition = Input[0].geoPosition;
//    Output.scalar = Input[0].scalar;
//    Output.normalCorners[0] = n1;
//    Output.normalCorners[1] = n2;
//    Output.normalCorners[2] = n3;
//    Output.sphericalPosCorners[0] = corner1;
//    Output.sphericalPosCorners[1] = corner2;
//    Output.sphericalPosCorners[2] = corner3;
//    Output.geoPosCorners[0] = Input[0].geoPosition;
//    Output.geoPosCorners[1] = Input[1].geoPosition;
//    Output.geoPosCorners[2] = Input[2].geoPosition;
//    Output.scalarCorners = vec3(Input[0].scalar, Input[1].scalar, Input[2].scalar);
//    Output.trianglePosition = vec3(1, 0, 0);
//    EmitVertex();
//
//    gl_Position = gl_in[1].gl_Position;
//    Output.flag = Input[1].flag;
//    Output.geoPosition = Input[1].geoPosition;
//    Output.scalar = Input[1].scalar;
//    Output.normalCorners[0] = n1;
//    Output.normalCorners[1] = n2;
//    Output.normalCorners[2] = n3;
//    Output.sphericalPosCorners[0] = corner1;
//    Output.sphericalPosCorners[1] = corner2;
//    Output.sphericalPosCorners[2] = corner3;
//    Output.geoPosCorners[0] = Input[0].geoPosition;
//    Output.geoPosCorners[1] = Input[1].geoPosition;
//    Output.geoPosCorners[2] = Input[2].geoPosition;
//    Output.scalarCorners = vec3(Input[0].scalar, Input[1].scalar, Input[2].scalar);
//    Output.trianglePosition = vec3(0, 1, 0);
//    EmitVertex();
//
//    gl_Position = gl_in[2].gl_Position;
//    Output.flag = Input[2].flag;
//    Output.geoPosition = Input[2].geoPosition;
//    Output.scalar = Input[2].scalar;
//    Output.normalCorners[0] = n1;
//    Output.normalCorners[1] = n2;
//    Output.normalCorners[2] = n3;
//    Output.sphericalPosCorners[0] = corner1;
//    Output.sphericalPosCorners[1] = corner2;
//    Output.sphericalPosCorners[2] = corner3;
//    Output.geoPosCorners[0] = Input[0].geoPosition;
//    Output.geoPosCorners[1] = Input[1].geoPosition;
//    Output.geoPosCorners[2] = Input[2].geoPosition;
//    Output.scalarCorners = vec3(Input[0].scalar, Input[1].scalar, Input[2].scalar);
//    Output.trianglePosition = vec3(0, 0, 1);
//    EmitVertex();
//}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

uniform sampler1D transferFunction; // 1D transfer function
uniform float     scalarMinimum;    // min/max data values to scale to 0..1
uniform float     scalarMaximum;

uniform bool      isCyclicGrid;   // indicates whether the grid is cyclic or not
uniform float     leftGridLon;    // leftmost longitude of the grid
uniform float     eastGridLon;    // eastmost longitude of the grid

uniform int       interpolationMethod;


shader FSmain(in VStoFS Input, out vec4 fragColour)
{
    // Discard the element if it is outside the model domain (no scalar value).
    if (Input.flag < 0. || Input.geoPosition.x < bboxLons.x || Input.geoPosition.x > bboxLons.y)
    {
        discard;
    }

    // In the case of the rendered region falling apart into disjunct region
    // discard fragments between seperated regions.
    // (cf. computeRenderRegionParameters of MNWP2DHorizontalActorVariable in
    // nwpactorvariable.cpp).
    if (!isCyclicGrid
            && (mod(Input.geoPosition.x - leftGridLon, 360.) >= (eastGridLon - leftGridLon)))
    {
        discard;
    }

    // Scale the scalar range to 0..1.
    float scalar_ = (Input.scalar - scalarMinimum) / (scalarMaximum - scalarMinimum);

    // Fetch colour from the transfer function and apply shading term.
    fragColour = texture(transferFunction, scalar_);
}

//shader FSinterpolated(in VStoFS Input, out vec4 fragColour)
//{
//    // Discard the element if it is outside the model domain (no scalar value).
//    if (Input.flag < 0. || Input.geoPosition.x < bboxLons.x || Input.geoPosition.x > bboxLons.y)
//    {
//        discard;
//    }
//
//    // In the case of the rendered region falling apart into disjunct region
//    // discard fragments between seperated regions.
//    // (cf. computeRenderRegionParameters of MNWP2DHorizontalActorVariable in
//    // nwpactorvariable.cpp).
//    if (!isCyclicGrid
//            && (mod(Input.geoPosition.x - leftGridLon, 360.) >= (eastGridLon - leftGridLon)))
//    {
//        discard;
//    }
//
//    float scalar_ = Input.scalar;
//    if (interpolationMethod == LINEAR)
//    {
//        scalar_ = Input.trianglePosition.x * Input.scalarCorners.x
//                  + Input.trianglePosition.y * Input.scalarCorners.y
//                  + Input.trianglePosition.z * Input.scalarCorners.z;
//        //fragColour = vec4(Input.trianglePosition, 1);
//    }
//    else if (interpolationMethod == SPHERICAL)
//    {
//        vec3 position = toSpherical(Input.geoPosition);
//
//        //compute spherical barycentric coordinates
//        float b1 = dot(position, Input.normalCorners[0]) / dot(Input.sphericalPosCorners[0], Input.normalCorners[0]);
//        float b2 = dot(position, Input.normalCorners[1]) / dot(Input.sphericalPosCorners[1], Input.normalCorners[1]);
//        float b3 = dot(position, Input.normalCorners[2]) / dot(Input.sphericalPosCorners[2], Input.normalCorners[2]);
//
//        //compute spherically interpolated scalar value
//        scalar_ = b1 * Input.scalarCorners.x
//                  + b2 * Input.scalarCorners.y
//                  + b3 * Input.scalarCorners.z;
//        //fragColour = vec4(b1, b2, b3, 1);
//    }
//    else if (interpolationMethod == DISTANCE)
//    {
//        float powerParameter = -2;
//
//        float h1 = distanceUnitSphere(Input.geoPosCorners[0], Input.geoPosition);
//        float h2 = distanceUnitSphere(Input.geoPosCorners[1], Input.geoPosition);
//        float h3 = distanceUnitSphere(Input.geoPosCorners[2], Input.geoPosition);
//        if (abs(h1) < 9.e-14)
//        {
//            scalar_ = Input.scalarCorners.x;
//            //fragColour = vec4(1, 0, 0, 1);
//        }
//        else if (abs(h2) < 9.e-14)
//        {
//            scalar_ = Input.scalarCorners.y;
//            //fragColour = vec4(0, 1, 0, 1);
//        }
//        else if (abs(h3) < 9.e-14)
//        {
//            scalar_ = Input.scalarCorners.z;
//            //fragColour = vec4(0, 0, 1, 1);
//        }
//        else
//        {
//            float p1 = pow(h1, powerParameter);
//            float p2 = pow(h2, powerParameter);
//            float p3 = pow(h3, powerParameter);
//
//            float sum = p1 + p2 + p3;
//
//            float w1 = p1 / sum;
//            float w2 = p2 / sum;
//            float w3 = p3 / sum;
//
//            scalar_ = w1 * Input.scalarCorners.x
//                    + w2 * Input.scalarCorners.y
//                    + w3 * Input.scalarCorners.z;
//            //fragColour = vec4(w1, w2, w3, 1);
//        }
//    }
//
//    // Scale the scalar range to 0..1.
//    scalar_ = (scalar_ - scalarMinimum) / (scalarMaximum - scalarMinimum);
//
//    // Fetch colour from the transfer function and apply shading term.
//    fragColour = texture(transferFunction, scalar_);
//}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Standard
{
    vs(420)=VSproj();
    fs(420)=FSmain();
};

program Interpolated
{
    vs(420)=VSmain();
    tc(420)=TCmain() : out(vertices = 4);
    te(420)=TEmain() : in(quads, equal_spacing, cw);
    fs(420)=FSmain();
};



