/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**  Copyright 2015 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

const float M_PI = 3.1415926535897932384626433832795;
const float DEG2RAD = M_PI / 180.0;
const float RAD2DEG = 180.0 / M_PI;

const int   EQUIRECTANGULAR = 0;
const int   STEREOGRAPHIC = 1;

const int   LINEAR_PERSPECTIVE = 0;
const int   LINEAR = 1;
const int   SPHERICAL = 2;
const int   DISTANCE = 3;

const int   PROJ_FWD = 1;
const int   PROJ_INV = -1;

const float DEFAULT_RADIUS = 90.0;

// Basemap vertex element.
struct ProjectionPoint
{
    vec2 mapProjection;
    vec2 geoPosition;
};

/*****************************************************************************
 ***                               UTIL
 *****************************************************************************/

// convert position to latitude values between -90 and 90
// and longitude values between -180 and 180.
vec2 adaptPosition(vec2 position)
{
    position.x = mod(position.x, 360);
    if (position.x > 180.)
    {
        position.x -= 360.;
    }
    else if (position.x < -180.)
    {
        position.x += 360.;
    }

    position.y = mod(position.y, 360);
    if (position.y > 180.)
    {
        position.y -= 360.;
    }
    else if (position.y < -180.)
    {
        position.y += 360.;
    }
    if (position.y > 90.)
    {
        position.y = 90.;
    }
    else if (position.y < -90.)
    {
        position.y = -90.;
    }

    return position;
}

/*****************************************************************************
 ***                             METHODS
 *****************************************************************************/

// projects geographic coordinates stereographic to a 2D map with x and y coordinates
// between -radius and +radius. The center of projection is arbitrary.
vec2 geoToMapStereographic(vec2 position, vec2 center, float radius)
{
    // precompute often used values.
    float sinCentralLat = sin(DEG2RAD * center.y);
    float cosCentralLat = cos(DEG2RAD * center.y);
    float sinLat = sin(DEG2RAD * position.y);
    float cosLat = cos(DEG2RAD * position.y);
    float cosDeltaLon = cos(DEG2RAD * (position.x - center.x));

    //project lon/lat stereographic.
    float scale = radius / (1 + sinCentralLat * sinLat
                  + cosCentralLat * cosLat * cosDeltaLon);
    float mapX = scale * cosLat * sin(DEG2RAD * (position.x - center.x));
    float mapY = scale * (cosCentralLat * sinLat - sinCentralLat * cosLat * cosDeltaLon);

    return vec2(mapX, mapY);
}

// projects map coordinates stereographic to geographic coordinates
// with latitudes between -90 and 90 and longitudes always between -180 and 180.
// The center of projection is arbitrary.
vec2 mapToGeoStereographic(vec2 position, vec2 center, float radius)
{
    //convert to polar coordinates.
    float distance = length(position);
    float alpha = 2 * atan(distance, radius);

    //precompute often used values.
    float cosAlpha = cos(alpha);
    float sinAlpha = sin(alpha);
    float cosCentralLat = cos(DEG2RAD * center.y);
    float sinCentralLat = sin(DEG2RAD * center.y);

    float lat = RAD2DEG * asin(cosAlpha * sinCentralLat + position.y * sinAlpha * cosCentralLat / distance);
    float denom = distance * cosCentralLat * cosAlpha - position.y * sinCentralLat * sinAlpha;
    float lon = center.x + RAD2DEG * atan(position.x * sinAlpha, denom);
    return adaptPosition(vec2(lon, lat));
}

// project a geograpical coordinates to a 2D map position
// using a equirectangular projection.
// The center of projection is arbitrary.
vec2 geoToMapEquirectangular(vec2 position, vec2 center)
{
    // Early break for rotation values with no effect.
    if (center.x == 0 && center.y == 0)
    {
        return position;
    }

    // adapt center.
    center = vec2(center.x, center.y + 90);

    // Get longitude and latitude from point.
    float lon = position.x;
    float lat = position.y;

    // Convert degrees to radians.
    float poleLatRad = DEG2RAD * center.y;
    float poleLonRad = DEG2RAD * center.x;
    float lonRad = DEG2RAD * lon;
    float latRad = DEG2RAD * lat;

    // Compute sinus and cosinus of some coordinates since they are needed more
    // often later on.
    float sinPoleLat = sin(poleLatRad);
    float cosPoleLat = cos(poleLatRad);

    // Apply the transformation (conversation to Cartesian coordinates and  two
    // rotations; difference to original code: no use of pollam).

    float x = sinPoleLat * cos(latRad) * cos(lonRad - poleLonRad) - cosPoleLat * sin(latRad);
    float y = sin(lonRad - poleLonRad) * cos(latRad);
    float z = cosPoleLat * cos(latRad) * cos(lonRad - poleLonRad)
              + sinPoleLat * sin(latRad);

    // Avoid invalid values for z (Might occure due to inaccuracies in
    // computations).
    z = max(-1., min(1., z));

    // Too small values can lead to numerical problems in method atans2.
    if (abs(x) < 1.0e-20)
    {
        x = 1.0e-20;
    }

    // Compute spherical coordinates from Cartesian coordinates and convert
    // radians to degrees.

    position.x = RAD2DEG * atan(y, x);
    position.y = RAD2DEG * asin(z);

    return position;
}

// Project a position on the map to geographical Lon/Lat
// using an equirectangular projection
// with an arbitrary center of projection.
vec2 mapToGeoEquirectangular(vec2 position, vec2 center)
{
    // Early break for rotation values with no effect.
    if (center.x == 0. && center.y == 0)
    {
        return position;
    }

    position = adaptPosition(position);
    center = vec2(-180 + center.x, 90 - center.y);

    float result = 0.0f;

    // Get longitude and latitude from position.
    float rotLon = position.x;
    float rotLat = position.y;

    // Convert degrees to radians.
    float poleLatRad = DEG2RAD * center.y;
    float poleLonRad = DEG2RAD * center.x;
    float rotLonRad = DEG2RAD * rotLon;

    // Compute sinus and cosinus of some coordinates since they are needed more
    // often later on.
    float sinPoleLat = sin(poleLatRad);
    float cosPoleLat = cos(poleLatRad);
    float sinRotLatRad = sin(DEG2RAD * rotLat);
    float cosRotLatRad = cos(DEG2RAD * rotLat);
    float cosRotLonRad = cos(DEG2RAD * rotLon);

    // Apply the transformation (conversation to Cartesian coordinates and  two
    // rotations; difference to original code: no use of polgam).

    float x =
            (cos(poleLonRad) * (((-sinPoleLat) * cosRotLonRad * cosRotLatRad)
                                + (cosPoleLat * sinRotLatRad)))
            + (sin(poleLonRad) * sin(rotLonRad) * cosRotLatRad);
    float y =
            (sin(poleLonRad) * (((-sinPoleLat) * cosRotLonRad * cosRotLatRad)
                                + (cosPoleLat * sinRotLatRad)))
            - (cos(poleLonRad) * sin(rotLonRad) * cosRotLatRad);
    float z = cosPoleLat * cosRotLatRad * cosRotLonRad
            + sinPoleLat * sinRotLatRad;

    // Avoid invalid values for z (Might occure due to inaccuracies in
    // computations).
    z = max(-1., min(1., z));

    // Compute spherical coordinates from Cartesian coordinates and convert
    // radians to degrees.

    if ( abs(x) > 0.f )
    {
        result = RAD2DEG * atan(y, x);
    }
    if ( abs(result) < 9.e-14 )
    {
        result = 0.f;
    }

    position.x = result;
    position.y = RAD2DEG * (asin(z));

    return position;
}

// Project a geographical position to a map position according to projection Method.
vec2 projectGeoToMap(vec2 position, vec2 center, int projectionMethod){

    if(projectionMethod == EQUIRECTANGULAR)
    {
        return geoToMapEquirectangular(position, center);
    }
    else if(projectionMethod == STEREOGRAPHIC)
    {
        return geoToMapStereographic(position, center, DEFAULT_RADIUS);
    }
    return vec2(0,0);
}

// Project a map projection to a geographical position according to projection Method.
vec2 projectMapToGeo(vec2 position, vec2 center, int projectionMethod){

    if(projectionMethod == EQUIRECTANGULAR)
    {
        return mapToGeoEquirectangular(position, center);
    }
    else if(projectionMethod == STEREOGRAPHIC)
    {
        return mapToGeoStereographic(position, center, DEFAULT_RADIUS);
    }
    return vec2(0,0);
}

// returns the nearest longitudinal standard parallel of no distortion
float getNearestLon(vec2 currentPosition, vec2 previousPosition, vec2 centerLons){
    // compute the distances
    float dist1 = abs(centerLons.y - currentPosition.x);
    float dist2 = abs(centerLons.y + 360. - currentPosition.x);
    float dist3 = abs(centerLons.x - currentPosition.x);
    float dist4 = abs(centerLons.x + 360. - currentPosition.x);

    float dist5 = abs(centerLons.y - previousPosition.x);
    float dist6 = abs(centerLons.y + 360. - previousPosition.x);
    float dist7 = abs(centerLons.x - previousPosition.x);
    float dist8 = abs(centerLons.x + 360. - previousPosition.x);

    // find the minimum
    float distMin = min(min(dist1, dist2), min(dist3, dist4));
    distMin = min(distMin, min(min(dist5, dist6), min(dist7, dist8)));
    // if the minimum is the second standard parallel
    if (distMin == dist1 || distMin == dist2 || distMin == dist5
            || distMin == dist6)
    {
        return centerLons.y;
    }
    return centerLons.x;
}

// checks whether the connection between to points is valid or spanning the entire domain
bool validConnection(ProjectionPoint previousPosition,
                     ProjectionPoint currentPosition,
                     vec2 center,
                     int projectionMethod){
    // only check the points if they are on different sides of the central longitude
    if ((currentPosition.mapProjection.x >= 0. && previousPosition.mapProjection.x <= 0.)
            || (currentPosition.mapProjection.x <= 0. && previousPosition.mapProjection.x >= 0.))
    {
        // get the nearest central longitude
        float nearestLon = getNearestLon(currentPosition.geoPosition,
                                         previousPosition.geoPosition,
                                         vec2(center.x, center.x < 0 ? center.x + 180 : center.x - 180));
        // if the connection is going through the center it is ok
        // otherwise it is spanning from the left to the right
        vec2 position = vec2(nearestLon, currentPosition.geoPosition.y);
        vec2 projection = projectGeoToMap(position, center, projectionMethod);
        if(int(projection.x) != 0)
        {
            return false;
        }
        /*if (int(nearestLon) != int(center.x))
        {
            return false;
        }*/
    }
    if (abs(previousPosition.mapProjection.x - currentPosition.mapProjection.x) > 90)
    {
        return false;
    }
    if (abs(previousPosition.mapProjection.y - currentPosition.mapProjection.y) > 90)
    {
        return false;
    }
    return true;
}

vec3 toSpherical(vec2 geoPosition)
{
    float x = cos(DEG2RAD * geoPosition.y) * cos(DEG2RAD * geoPosition.x);
    float y = cos(DEG2RAD * geoPosition.y) * sin(DEG2RAD * geoPosition.x);
    float z = sin(DEG2RAD * geoPosition.y);
    return vec3(x, y, z);
}

float haversin(float radians)
{
    float sinValue = sin(radians / 2.);
    return sinValue * sinValue;
}


float distanceUnitSphere(vec2 fromPosition, vec2 toPosition)
{
    const float deltaLon = DEG2RAD * (toPosition.x - fromPosition.x);
    const float deltaLat = DEG2RAD * (toPosition.y - fromPosition.y);

    // Compute distance using the haversine formula.
    const float havSinAlpha = haversin(deltaLat) +
                              cos(DEG2RAD * fromPosition.y) * cos(DEG2RAD * toPosition.y) * haversin(deltaLon);

    return 2. * asin(sqrt(havSinAlpha));
}

float slerp(float x, float y, float t)
{
    float omega = acos(x * y);
    return (sin((1 - t) * omega)/sin(omega)) * x + (sin(t * omega)/sin(omega)) * y;
}