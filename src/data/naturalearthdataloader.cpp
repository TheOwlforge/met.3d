/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2015-2017 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "naturalearthdataloader.h"

#define _USE_MATH_DEFINES

// standard library imports
#include <iostream>
#include <math.h>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QVector>
#include <QVector2D>
#include <proj.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"

using namespace std;


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNaturalEarthDataLoader::MNaturalEarthDataLoader()
{
    GDALAllRegister();
}

MNaturalEarthDataLoader::~MNaturalEarthDataLoader()
{
    for (int i = 0; i < gdalDataSet.size(); i++) GDALClose(gdalDataSet[i]);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNaturalEarthDataLoader::setDataSources(
    QString coastlinesfile, QString borderlinesfile)
{
    // Remove existing datasources.
    for (int i = 0; i < gdalDataSet.size(); i++) GDALClose(gdalDataSet[i]);

    // We currently have 2 data sources.
    gdalDataSet.resize(2);

    // Open the coastlines shapefile.
    gdalDataSet[COASTLINES] = (GDALDataset *) GDALOpenEx(
        coastlinesfile.toStdString().c_str(),
        GDAL_OF_VECTOR, NULL, NULL, NULL);
    if (gdalDataSet[COASTLINES] == NULL)
    {
        QString msg = QString("ERROR: cannot open coastlines file %1")
            .arg(coastlinesfile);
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        throw MInitialisationError(msg.toStdString(), __FILE__, __LINE__);
    }

    gdalDataSet[BORDERLINES] = (GDALDataset *) GDALOpenEx(
        borderlinesfile.toStdString().c_str(),
        GDAL_OF_VECTOR, NULL, NULL, NULL);
    if (gdalDataSet[BORDERLINES] == NULL)
    {
        QString msg = QString("ERROR: cannot open borderlines file %1")
            .arg(borderlinesfile);
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        throw MInitialisationError(msg.toStdString(), __FILE__, __LINE__);
    }
}


void MNaturalEarthDataLoader::loadAndRotateLineGeometry(
    GeometryType type, QRectF bbox, QVector<QVector2D> *vertices,
    QVector<int> *startIndices, QVector<int> *count, bool append,
    QVector2D *standardLines, QVector2D *centralLons,
    MActor::ProjectionMethod projectionMethod)
{
    if (gdalDataSet.size() < 2)
    {
        QString msg = QString("ERROR: NaturalEarthDataLoader not yet "
                              "initilised.");
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        throw MInitialisationError(msg.toStdString(), __FILE__, __LINE__);
    }

    string typeStr = type == COASTLINES ? "COASTLINES": "BORDERLINES";
    LOG4CPLUS_DEBUG(mlog, "loading " << typeStr << " geometry..");

    if ( !append )
    {
        vertices->clear();
        startIndices->clear();;
        count->clear();
    }

    // NaturalEarth shapefiles only contain one layer. (Do shapefiles in
    // general contain only one layer?)
    OGRLayer *layer;
    layer = gdalDataSet[type]->GetLayer(0);

    // Create bounding box with coordinates mapped to the range [-180, 180] in
    // longitude without "wrapping" the bounding box around (i.e. the east
    // border must not be smaller than the west border). We apply this mapping
    // since the rotation maps all values to this range and the line geometries
    // are only defined on this range.
    QRectF bboxTransformed = bbox;
    bboxTransformed.setX(MMOD(bbox.x() + 180., 360.) - 180.);
    bboxTransformed.setWidth(min(bbox.width(), 180. - bboxTransformed.x()));
    OGRPolygon *bboxPolygon = getBBoxPolygon(&bboxTransformed);

    // Variables used to get rid of lines crossing the whole domain.
    // (Conntection of the right most and the left most vertex)
    ProjectionPoint prevPosition;
    ProjectionPoint currPosition;

    // Filter the layer on-load: Only load those geometries that intersect
    // with the bounding box.
    layer->SetSpatialFilter(bboxPolygon);

    // Loop over all features contained in the layer.
    layer->ResetReading();
    OGRFeature *feature;

    while ((feature = layer->GetNextFeature()) != NULL)
    {
        startIndices->append(vertices->size());
        prevPosition.geoPosition.setX(0.);

        QList<OGRLineString*> lineStrings;
        // Get the geometry associated with the current feature.
        getLineStringFeatures(&lineStrings, feature->GetGeometryRef());

        // Loop over the list and intersect the contained line strings with
        // the bounding box.
        for (int l = 0; l < lineStrings.size(); l++)
        {
            OGRLineString *lineString = lineStrings.at(l);

            // Compute intersection with bbox.
            OGRGeometry *iGeometry = lineString->Intersection(bboxPolygon);

            // The intersection can be either a single line string, or a
            // collection of line strings.

            if (iGeometry->getGeometryType() == wkbLineString)
            {
                // Get all points from the intersected line string and append
                // them to the "vertices" vector.
                OGRLineString *iLine = (OGRLineString *) iGeometry;
                int numLinePoints = iLine->getNumPoints();
                OGRRawPoint *v = new OGRRawPoint[numLinePoints];
                iLine->getPoints(v);

                for (int i = 0; i < numLinePoints; i++)
                {
                    currPosition.geoPosition.setX(v[i].x);
                    currPosition.geoPosition.setY(v[i].y);
                    currPosition.mapProjection = projectGeoToMap(&currPosition.geoPosition,
                                                                 standardLines,
                                                                 projectionMethod);
                    if (!validConnectionBetweenPositions(
                        &prevPosition, &currPosition,
                        centralLons, projectionMethod,
                        standardLines, 15))
                    {
                        // Start new line.
                        count->append(vertices->size() - startIndices->last());
                        startIndices->append(vertices->size());
                    }
                    prevPosition = currPosition;
                    vertices->append(currPosition.mapProjection);
                }
                delete[] v;
            }

            else if (iGeometry->getGeometryType() == wkbMultiLineString)
            {
                // Loop over all line strings in the collection, appending
                // their points to "vertices" as above.
                OGRGeometryCollection *geomCollection =
                    (OGRGeometryCollection *) iGeometry;

                for (int g = 0; g < geomCollection->getNumGeometries(); g++)
                {
                    OGRLineString *iLine =
                        (OGRLineString *) geomCollection->getGeometryRef(g);
                    int numLinePoints = iLine->getNumPoints();
                    OGRRawPoint *v = new OGRRawPoint[numLinePoints];
                    iLine->getPoints(v);

                    for (int i = 0; i < numLinePoints; i++)
                    {
                        currPosition.geoPosition.setX(v[i].x);
                        currPosition.geoPosition.setY(v[i].y);
                        currPosition.mapProjection = projectGeoToMap(&currPosition.geoPosition,
                                                                     standardLines,
                                                                     projectionMethod);
                        if (!validConnectionBetweenPositions(
                            &prevPosition, &currPosition,
                            centralLons, projectionMethod,
                            standardLines, 15))
                        {
                            // Start new line.
                            count->append(vertices->size() - startIndices->last());
                            startIndices->append(vertices->size());
                        }
                        prevPosition = currPosition;
                        vertices->append(currPosition.mapProjection);
                    }
                    delete[] v;
                }
            }
        } // while (lineStrings)

        OGRFeature::DestroyFeature(feature);
        count->append(vertices->size() - startIndices->last());
    }

    // Clean up.
    OGRGeometryFactory::destroyGeometry(bboxPolygon);

    // If we have loaded only a part of the line geometry, load the missing
    // geometry by calling the method again with an adapted bounding box.
    // This might happen if the bounding box "falls appart" into two segments
    // when mapped to the range [-180, 180] in longitude with regard to sphere
    // coordinates.
    if (bbox.width() > bboxTransformed.width())
    {
        double width = bboxTransformed.width();
        bboxTransformed.setX(-180.);
        bboxTransformed.setWidth(min(bbox.width(), 360.) - width);
        loadAndRotateLineGeometry(
            type, bboxTransformed, vertices, startIndices, count,
            true, standardLines, centralLons, projectionMethod);
    }
}


QVector2D MNaturalEarthDataLoader::projectGeoToMap(
    QVector2D *geoPosition, QVector2D* standardLines, MActor::ProjectionMethod projectionMethod)
{
    if (projectionMethod == MActor::EQUIRECTANGULAR)
    {
        return geoToMapEquirectangular(geoPosition, standardLines);
    }
    else if (projectionMethod == MActor::STEREOGRAPHIC)
    {
        return geoToMapStereographic(geoPosition, standardLines);
    }
    return *geoPosition;
}


QVector2D MNaturalEarthDataLoader::projectMapToGeo(
    QVector2D *mapProjection, QVector2D* standardLines, MActor::ProjectionMethod projectionMethod)
{
    if (projectionMethod == MActor::EQUIRECTANGULAR)
    {
        return mapToGeoEquirectangular(mapProjection, standardLines);
    }
    else if (projectionMethod == MActor::STEREOGRAPHIC)
    {
        return mapToGeoStereographic(mapProjection, standardLines);
    }
    return *mapProjection;
}


bool MNaturalEarthDataLoader::validConnectionBetweenPositions(
    ProjectionPoint* prevPosition, ProjectionPoint* currPosition,
    QVector2D *centralLons, MActor::ProjectionMethod projectionMethod,
    QVector2D *standardLines, uint threshold)
{
    bool result = true;

    // Check if connection between previous and current vertex crosses 0
    // (middle) since this might be a connection from left to right crossing
    // (nearly) the whole domain.
    if (((currPosition->mapProjection.x() >= 0. && prevPosition->mapProjection.x() <= 0.)
        || (currPosition->mapProjection.x() <= 0. && prevPosition->mapProjection.x() >= 0.)))
    {
        // Get centre longitude with overall shortest distance in longitudes to
        // either the current or previous vertex.
        // Use geo coordinates of current vertex
        // since it makes it easier to compare the coordinates.
        double centreLon = getNearestLon(currPosition->geoPosition.x(), prevPosition->geoPosition.x(), centralLons);

        // Rotate vertex on the nearest centre longitude and the latitude of the
        // current node.
        QVector2D geoPosition = QVector2D(centreLon, currPosition->geoPosition.y());
        QVector2D mapProjection = projectGeoToMap(&geoPosition, standardLines, projectionMethod);

        // If the rotated vertex maps to the centre (= 0) then the connection
        // between the current vertex and the previous is legitimate.
        if (int(mapProjection.x()) != 0)
        {
            result = false;
        }
    }
    if (abs(prevPosition->mapProjection.x() - currPosition->mapProjection.x()) > threshold)
    {
        result = false;
    }
    if (abs(prevPosition->mapProjection.y() - currPosition->mapProjection.y()) > threshold)
    {
        result = false;
    }

    return result;
}

double MNaturalEarthDataLoader::getNearestLon(
    double currLon, double prevLon, QVector2D *centralLons)
{
    double dist1 = abs(centralLons->y() - currLon);
    double dist2 = abs(centralLons->y() + 360. - currLon);
    double dist3 = abs(centralLons->x() - currLon);
    double dist4 = abs(centralLons->x() + 360. - currLon);

    double dist5 = abs(centralLons->y() - prevLon);
    double dist6 = abs(centralLons->y() + 360. - prevLon);
    double dist7 = abs(centralLons->x() - prevLon);
    double dist8 = abs(centralLons->x() + 360. - prevLon);

    double distMin = min(min(dist1, dist2), min(dist3, dist4));
    distMin = min(distMin, min(min(dist5, dist6), min(dist7, dist8)));
    if (distMin == dist1 || distMin == dist2 || distMin == dist5
        || distMin == dist6)
    {
        return centralLons->y();
    }
    return centralLons->x();
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MNaturalEarthDataLoader::getLineStringFeatures(
    QList<OGRLineString *> *lineStrings, OGRGeometry *geometry)
{
    // For coastlines, borderlines etc. we are only interested in line
    // string features. Note that all lines that intersect with the
    // bounding box are returned here. This includes lines that lie only
    // partially within the bounding box. Hence we have to compute the
    // intersection of each line with the bounding box.

    // If the geometry is of type wkbLineString or wkbMultiLineString,
    // place the contained line strings in a list with data to be
    // processed.
    if (geometry != NULL)
    {
        OGRwkbGeometryType gType = wkbFlatten(geometry->getGeometryType());

        if (gType == wkbLineString)
        {
            lineStrings->append((OGRLineString *) geometry);
        }

        else if (gType == wkbMultiLineString)
        {
            OGRGeometryCollection *gc = (OGRGeometryCollection *) geometry;
            for (int g = 0; g < gc->getNumGeometries(); g++)
                lineStrings->append((OGRLineString *) gc->getGeometryRef(g));
        }

    } // geometry != NULL
}

OGRPolygon *MNaturalEarthDataLoader::getBBoxPolygon(QRectF *bbox)
{
    // Create a bounding box geometry: NOTE that this needs to be polygon -- if
    // a line string or ring is used, the Intersection() method used below will
    // only return the points that actually intersect the line, i.e. that are
    // on the line.
    float leftlon = bbox->x();
    float lowerlat = bbox->y();
    float rightlon = bbox->x() + bbox->width();
    float upperlat = bbox->y() + bbox->height();

    OGRLinearRing bboxRing;
    bboxRing.addPoint(leftlon, lowerlat);
    bboxRing.addPoint(rightlon, lowerlat);
    bboxRing.addPoint(rightlon, upperlat);
    bboxRing.addPoint(leftlon, upperlat);
    bboxRing.addPoint(leftlon, lowerlat);
    // OGRPolygon *bboxPolygon = new OGRPolygon(); causes problems on windows
    OGRPolygon *bboxPolygon = dynamic_cast<OGRPolygon *>
    (OGRGeometryFactory::createGeometry(OGRwkbGeometryType::wkbPolygon));
    bboxPolygon->addRing(&bboxRing);
    return bboxPolygon;
}

static const double DEG2RAD = M_PI / 180.0;
static const double RAD2DEG = 180.0 / M_PI;
static const double DEFAULT_RADIUS = 90.0;

QVector2D MNaturalEarthDataLoader::geoToMapStereographic(
    QVector2D *geoPosition, QVector2D *standardLines)
{
    // precompute often used values.
    double sinCentralLat = sin(DEG2RAD * standardLines->y());
    double cosCentralLat = cos(DEG2RAD * standardLines->y());
    double sinLat = sin(DEG2RAD * geoPosition->y());
    double cosLat = cos(DEG2RAD * geoPosition->y());
    double cosDeltaLon = cos(DEG2RAD * (geoPosition->x() - standardLines->x()));

    //project lon/lat stereographic.
    double scale = DEFAULT_RADIUS / (1 + sinCentralLat * sinLat
                                     + cosCentralLat * cosLat * cosDeltaLon);
    double mapX = scale * cosLat * sin(DEG2RAD * (geoPosition->x() - standardLines->x()));
    double mapY = scale * (cosCentralLat * sinLat - sinCentralLat * cosLat * cosDeltaLon);

    return QVector2D(mapX, mapY);
}

QVector2D MNaturalEarthDataLoader::mapToGeoStereographic(QVector2D *mapProjection, QVector2D *standardLines)
{
    //convert to polar coordinates.
    float distance = mapProjection->length();
    float alpha = 2 * atan2(distance, DEFAULT_RADIUS);

    //precompute often used values.
    float cosAlpha = cos(alpha);
    float sinAlpha = sin(alpha);
    float cosCentralLat = cos(DEG2RAD * standardLines->y());
    float sinCentralLat = sin(DEG2RAD * standardLines->y());

    float lat = RAD2DEG * asin(cosAlpha * sinCentralLat + (mapProjection->y() * sinAlpha * cosCentralLat) / distance);
    float denom = distance * cosCentralLat * cosAlpha - mapProjection->y() * sinCentralLat * sinAlpha;
    float lon = standardLines->x() + RAD2DEG * atan2(mapProjection->x() * sinAlpha, denom);

    QVector2D result = QVector2D(lon, lat);
    return adaptPosition(&result);
}

// Parts of the following method have been ported from the C implementation of
// the methods 'lam_to_lamrot' and 'phi_to_phirot'. The original code has been
// published under GNU GENERAL PUBLIC LICENSE Version 2, June 1991.
// source: https://code.zmaw.de/projects/cdo/files  [Version 1.8.1]

// Original code:

// static
// double lam_to_lamrot(double phi, double rla, double polphi, double pollam)
// {
//   /*
//     Umrechnung von rla (geo. System) auf rlas (rot. System)

//     phi    : Breite im geographischen System (N>0)
//     rla    : Laenge im geographischen System (E>0)
//     polphi : Geographische Breite des Nordpols des rot. Systems
//     pollam : Geographische Laenge des Nordpols des rot. Systems

//     result : Rotierte Laenge
//   */
//   double zsinpol = sin(DEG2RAD*polphi);
//   double zcospol = cos(DEG2RAD*polphi);
//   double zlampol =     DEG2RAD*pollam;

//   if ( rla > 180.0 ) rla -= 360.0;

//   double zrla = DEG2RAD*rla;
//   double zphi = DEG2RAD*phi;

//   double zarg1  = - sin(zrla-zlampol)*cos(zphi);
//   double zarg2  = - zsinpol*cos(zphi)*cos(zrla-zlampol)+zcospol*sin(zphi);

//   if ( fabs(zarg2) < 1.0e-20 ) zarg2 = 1.0e-20;

//   return RAD2DEG*atan2(zarg1,zarg2);
// }

// static
// double phi_to_phirot(double phi, double rla, double polphi, double pollam)
// {
//   /*
//     Umrechnung von phi (geo. System) auf phis (rot. System)

//     phi    : Breite im geographischen System (N>0)
//     rla    : Laenge im geographischen System (E>0)
//     polphi : Geographische Breite des Nordpols des rot. Systems
//     pollam : Geographische Laenge des Nordpols des rot. Systems

//     result : Rotierte Breite
//   */
//   double zsinpol = sin(DEG2RAD*polphi);
//   double zcospol = cos(DEG2RAD*polphi);
//   double zlampol =     DEG2RAD*pollam;

//   double zphi = DEG2RAD*phi;
//   if ( rla > 180.0 ) rla -= 360.0;
//   double zrla = DEG2RAD*rla;

//   double zarg = zcospol*cos(zphi)*cos(zrla-zlampol) + zsinpol*sin(zphi);

//   return RAD2DEG*asin(zarg);
// }

QVector2D MNaturalEarthDataLoader::geoToMapEquirectangular(
    QVector2D *geoPosition, QVector2D *standardLines)
{
    double poleLon = standardLines->x();
    double poleLat = standardLines->y();
    // Early break for rotation values with no effect.
    if (poleLon == 0. && poleLat == 0.)
    {
        return QVector2D(geoPosition->x(), geoPosition->y());
    }
    poleLat = 90 - poleLat;
    poleLon = -180 + poleLon;

    // Get longitude and latitude from point.
    double lon = geoPosition->x();
    double lat = geoPosition->y();

    // Convert degrees to radians.
    double poleLatRad = DEG2RAD * poleLat;
    double poleLonRad = DEG2RAD * poleLon;
    double lonRad = DEG2RAD * lon;
    double latRad = DEG2RAD * lat;

    // Compute sinus and cosinus of some coordinates since they are needed more
    // often later on.
    double sinPoleLat = sin(poleLatRad);
    double cosPoleLat = cos(poleLatRad);

    // Apply the transformation (conversation to Cartesian coordinates and  two
    // rotations; difference to original code: no use of pollam).

    double x = ((-sinPoleLat) * cos(latRad) * cos(lonRad - poleLonRad))
        + (cosPoleLat * sin(latRad));
    double y = (-sin(lonRad - poleLonRad)) * cos(latRad);
    double z = (cosPoleLat * cos(latRad) * cos(lonRad - poleLonRad))
        + (sinPoleLat * sin(latRad));

    // Avoid invalid values for z (Might occure due to inaccuracies in
    // computations).
    z = max(-1., min(1., z));

    // Too small values can lead to numerical problems in method atans2.
    if ( std::abs(x) < 1.0e-20 )
    {
        x = 1.0e-20;
    }

    // Compute spherical coordinates from Cartesian coordinates and convert
    // radians to degrees.
    return QVector2D(RAD2DEG * (atan2(y, x)), RAD2DEG * (asin(z)));
}


// Parts of the following method have been ported from the C implementation of
// the methods 'lamrot_to_lam' and 'phirot_to_phi'. The original code has been
// published under GNU GENERAL PUBLIC LICENSE Version 2, June 1991.
// source: https://code.zmaw.de/projects/cdo/files  [Version 1.8.1]
// Necessary code duplicate in basemap.fx.glsl .

// Original code:

// double lamrot_to_lam(double phirot, double lamrot, double polphi, double pollam, double polgam)
// {
//   /*
//     This function converts lambda from one rotated system to lambda in another system.
//     If the optional argument polgam is present, the other system can also be a rotated one,
//     where polgam is the angle between the two north poles.
//     If polgam is not present, the other system is the real geographical system.

//     phirot : latitude in the rotated system
//     lamrot : longitude in the rotated system (E>0)
//     polphi : latitude of the rotated north pole
//     pollam : longitude of the rotated north pole

//     result : longitude in the geographical system
//   */
//   double zarg1, zarg2;
//   double zgam;
//   double result = 0;

//   double zsinpol = sin(DEG2RAD*polphi);
//   double zcospol = cos(DEG2RAD*polphi);

//   double zlampol = DEG2RAD*pollam;
//   double zphirot = DEG2RAD*phirot;
//   if ( lamrot > 180.0 ) lamrot -= 360.0;
//   double zlamrot = DEG2RAD*lamrot;

//   if ( fabs(polgam) > 0 )
//     {
//       zgam  = -DEG2RAD*polgam;
//       zarg1 = sin(zlampol) *
//  	     (- zsinpol*cos(zphirot) * (cos(zlamrot)*cos(zgam) - sin(zlamrot)*sin(zgam))
//  	      + zcospol*sin(zphirot))
// 	 - cos(zlampol)*cos(zphirot) * (sin(zlamrot)*cos(zgam) + cos(zlamrot)*sin(zgam));

//       zarg2 = cos(zlampol) *
//  	     (- zsinpol*cos(zphirot) * (cos(zlamrot)*cos(zgam) - sin(zlamrot)*sin(zgam))
//	      + zcospol*sin(zphirot))
//	 + sin(zlampol)*cos(zphirot) * (sin(zlamrot)*cos(zgam) + cos(zlamrot)*sin(zgam));
//      }
//   else
//     {
//       zarg1 = sin(zlampol)*(- zsinpol*cos(zlamrot)*cos(zphirot)  +
//      		               zcospol*             sin(zphirot)) -
//	       cos(zlampol)*           sin(zlamrot)*cos(zphirot);
//       zarg2 = cos(zlampol)*(- zsinpol*cos(zlamrot)*cos(zphirot)  +
//                               zcospol*             sin(zphirot)) +
//               sin(zlampol)*           sin(zlamrot)*cos(zphirot);
//     }

//   if ( fabs(zarg2) > 0 ) result = RAD2DEG*atan2(zarg1, zarg2);
//   if ( fabs(result) < 9.e-14 ) result = 0;

//   return result;
// }

// double phirot_to_phi(double phirot, double lamrot, double polphi, double polgam)
// {
//   /*
//     This function converts phi from one rotated system to phi in another
//     system. If the optional argument polgam is present, the other system
//     can also be a rotated one, where polgam is the angle between the two
//     north poles.
//     If polgam is not present, the other system is the real geographical
//     system.

//     phirot : latitude in the rotated system
//     lamrot : longitude in the rotated system (E>0)
//     polphi : latitude of the rotated north pole
//     polgam : angle between the north poles of the systems

//     result : latitude in the geographical system
//   */
//   double zarg;
//   double zgam;

//   double zsinpol = sin(DEG2RAD*polphi);
//   double zcospol = cos(DEG2RAD*polphi);

//   double zphirot   = DEG2RAD*phirot;
//   if ( lamrot > 180.0 ) lamrot -= 360.0;
//   double zlamrot   = DEG2RAD*lamrot;

//   if ( fabs(polgam) > 0 )
//     {
//       zgam = -DEG2RAD*polgam;
//       zarg = zsinpol*sin(zphirot) +
//              zcospol*cos(zphirot)*(cos(zlamrot)*cos(zgam) - sin(zgam)*sin(zlamrot));
//     }
//   else
//     zarg   = zcospol*cos(zphirot)*cos(zlamrot) + zsinpol*sin(zphirot);

//   return RAD2DEG*asin(zarg);
// }

QVector2D MNaturalEarthDataLoader::mapToGeoEquirectangular(
    QVector2D *mapProjection, QVector2D *standardLines)
{
    double poleLon = standardLines->x();
    double poleLat = standardLines->y();
    // Early break for rotation values with no effect.
    if (poleLon == 0. && poleLat == 0.)
    {
        return QVector2D(mapProjection->x(), mapProjection->y());
    }

    double result = 0;

    // Get longitude and latitude from point.
    QVector2D adaptedPosition = adaptPosition(mapProjection);
    double rotLon = adaptedPosition.x();
    double rotLat = adaptedPosition.y();

    // Convert degrees to radians.
    double poleLatRad = DEG2RAD * poleLat;
    double poleLonRad = DEG2RAD * poleLon;
    double rotLonRad = DEG2RAD * rotLon;

    // Compute sinus and cosinus of some coordinates since they are needed more
    // often later on.
    double sinPoleLat = sin(poleLatRad);
    double cosPoleLat = cos(poleLatRad);
    double sinRotLatRad = sin(DEG2RAD * rotLat);
    double cosRotLatRad = cos(DEG2RAD * rotLat);
    double cosRotLonRad = cos(DEG2RAD * rotLon);

    // Apply the transformation (conversation to Cartesian coordinates and  two
    // rotations; difference to original code: no use of polgam).

    double x =
        (cos(poleLonRad) * (((-sinPoleLat) * cosRotLonRad * cosRotLatRad)
            + (cosPoleLat * sinRotLatRad)))
            + (sin(poleLonRad) * sin(rotLonRad) * cosRotLatRad);
    double y =
        (sin(poleLonRad) * (((-sinPoleLat) * cosRotLonRad * cosRotLatRad)
            + (cosPoleLat * sinRotLatRad)))
            - (cos(poleLonRad) * sin(rotLonRad) * cosRotLatRad);
    double z = cosPoleLat * cosRotLatRad * cosRotLonRad
        + sinPoleLat * sinRotLatRad;

    // Avoid invalid values for z (Might occure due to inaccuracies in
    // computations).
    z = max(-1., min(1., z));

    // Compute spherical coordinates from Cartesian coordinates and convert
    // radians to degrees.

    if ( std::abs(x) > 0 )
    {
        result = RAD2DEG * atan2(y, x);
    }
    if ( std::abs(result) < 9.e-14 )
    {
        result = 0;
    }

    return QVector2D(result, RAD2DEG * (asin(z)));
}

// convert position to latitude values between -90 and 90
// and longitude values between -180 and 180.
QVector2D MNaturalEarthDataLoader::adaptPosition(QVector2D *position)
{
    QVector2D result = QVector2D(position->x(), position->y());
    result.setX(fmod(result.x(), 360));
    if (result.x() > 180.)
    {
        result.setX(result.x() - 360.);
    }
    else if (result.x() < -180.)
    {
        result.setX(result.x() + 360.);
    }

    result.setY(fmod(result.y(), 360));
    if (result.y() > 180.)
    {
        result.setY(result.y() - 360.);
    }
    else if (result.y() < -180.)
    {
        result.setY(result.y() + 360.);
    }
    if (result.y() > 90.)
    {
        position->setY(90.);
    }
    else if (result.y() < -90.)
    {
        position->setY(-90.);
    }

    return result;
}

} // namespace Met3D
